# -*- encoding: utf-8 -*-

import torch
import numpy as np

class LinRegression:
    """A Simple Linear Regression Model, built from Scratch using PyTorch

    A simple model like the Linear-Regression is avaialable under ``nn.Linear``
    module of PyTorch. However, for learning purpose, this module
    implements the same from scratch using basic programming concepts and using
    the NumPy library to instantiate the weights and biases.
    """

    def __init__(
            self,
            x_train      : np.ndarray or torch.Tensor, # dtype='float32' or equivalent
            y_train      : np.ndarray or torch.Tensor, # dtype='float32' or equivalent
            input_shape  : int,
            output_shape : int,
            seed         : int   = None,
            l_rate       : float = 1e-5,
            epochs       : int   = 1e3,
            verbose      : bool  = True
        ):
        self.x_train = x_train
        self.y_train = y_train

        if type(self.x_train == np.ndarray): self.x_train = torch.from_numpy(self.x_train)
        if type(self.y_train == np.ndarray): self.y_train = torch.from_numpy(self.y_train)

        self.input_shape  = input_shape
        self.output_shape = output_shape

        # Initialize weights and bias
        self.seed    = seed # If true, set a seed using np.random.seed()
        if self.seed:
            np.random.seed(self.seed)

        self.bias    = self._init_bias
        self.weights = self._init_weights

        # Initialize learning rate, epochs and others
        self.l_rate  = l_rate
        self.epochs  = epochs
        self.verbose = verbose

        # History/Callbacks
        self._loss = []

    @property
    def _init_weights(self):
        """Initializes weights from the (input, output) shape"""

        return torch.randn(self.output_shape, self.input_shape, requires_grad = True)
    
    @property
    def _init_bias(self):
        """Initializes bias from the (input, output) shape"""

        return torch.randn(self.output_shape, requires_grad = True)

    def model(self):
        """Defines the Linear Regression Model given as ``y = wx + b``"""

        return self.x_train @ self.weights.t() + self.bias

    def model_loss(self, y_predicted : torch.Tensor):
        """Loss Function associated with the Model: MSE"""

        _difference = y_predicted - self.y_train
        return torch.sum( _difference ** 2 ) / _difference.numel()

    def fit(self):
        """Fit Linear Regression Model"""

        for _ in range(int(self.epochs) - 1):
            _predictions   = self.model()
            _calc_mse_loss = self.model_loss(_predictions)

            # Verbose, and generating History
            self._loss.append(_calc_mse_loss)
            if self.verbose & (_ % 10 == 0):
                print(f"Epoch: {_} Loss = {_calc_mse_loss}")

            _calc_mse_loss.backward() # we need to differentiate to update the weights and biases
            with torch.no_grad():
                self.bias    -= self.bias * self.l_rate
                self.weights -= self.weights * self.l_rate

                self.bias.grad.zero_()
                self.weights.grad.zero_()

        return self.model()

    @property
    def history(self):
        return {
            'Loss' : self._loss
        }
    