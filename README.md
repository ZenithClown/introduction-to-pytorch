<div align="center">

<h1 align = "center">
	Introduction to PyTorch <br>
	<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>
    <a href = "https://www.hackerrank.com/dPramanik"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/hackerrank.svg"/></a>
    <br>
    <a href = "https://jovian.ml/dpramanik"><img src="https://img.shields.io/badge/Jovian.ml-notebooks-%23cbd0d6"></a>
    <a href="https://zenithclown.gitlab.io/introduction-to-pytorch/"><img alt="PyPI Package" src="https://img.shields.io/badge/Pages-documentations-%23216e65?style=plastic&logo=gitlab"/></a>
</h1>

</div>

<p align="justify">Find all the Chapters/Sections in the following Links:</p>

- [Chapter 1: Basic Data Types and Gradient Calculations](https://jovian.ml/dpramanik/pytorch-1-basic-data-types-and-gradient-calculations)
- Chapter 2: Basic Machine Learning Models
  * [Linear Regression](https://jovian.ml/dpramanik/pytorch-2-linear-regression) Model built from Scratch using `PyTorch`.