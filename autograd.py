# -*- encoding: utf-8 -*-

import torch
import warnings
from torch.autograd import grad

class autograd:
    """Analogous to torch.autograd Calculates the n-th Order Derivative

    Defined a function `f(x)` for any input variable `x` of let highest power `p`,
    the derivative exists for `(1, 2, ..., p, p + 1)` and the value of `(p + 1)`-derivative
    is always zero.

    :type  output_: torch.Tensor
    :param output_: Function `f(x)`

    :type  input_: torch.Tensor
    :param input_: Input Variable `x`

    :type  order: int
    :param order: Required Order of Derivative

    :raises TypeError:  Derivative is only available for int
    :raises ValueError: Derivative does not exists for order < 1
    """

    def __init__(self, output_, input_, order):
        self.input_  = input_  # Input Variable
        self.output_ = output_ # Output Variable

        # order is only accepted of type = int and order >= 1
        self.order = order

        if type(self.order) != int:
            raise TypeError(f'Only int accepted, got {type(self.order)}')

        if self.order < 1:
            raise ValueError(f'{(self.order)}-th of any function does not Exists')

    @property
    def derivative(self):
        """Calculates the Derivative of the given Order"""
        _derivative = grad(self.output_, self.input_, create_graph = True)

        if self.order == 1:
            return _derivative

        for _ in range(self.order - 1):
            try:
                _derivative = grad(_derivative, self.input_, create_graph = True)
            except RuntimeError as err:
                warnings.warn(f"Highest Power is {_} but required derivative of {self.order}: {err}", RuntimeWarning)
                return torch.tensor(0.)

        return _derivative