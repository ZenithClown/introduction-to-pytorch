Chapter 1: Basic Data Types and Gradient Calculations
=====================================================

A basic introduction to data types and creating ``tensor`` objects using PyTorch.
The Gradient Descend Algorithm is explored here, and for finding higher-order
differentials of a given funtion, the :mod:`autograd` module is built.

.. toctree::
   :maxdepth: 4

   autograd.rst
