Chapter 2: Basic Machine Learning Models
========================================

A small introduction to basic machine learning models and learning how to actually
build an algorithm from scratch.

.. toctree::
   :maxdepth: 4

   LinearRegression.rst
