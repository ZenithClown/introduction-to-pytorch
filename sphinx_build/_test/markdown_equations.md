Let a simple function be defined by the following equation:

$$
    f(x) = a_1 x^p + a_2 x^{p - 1} + ... + a_{p - 1} x^2 + a_p x + a_0
$$

where, $a_0, a_1, ..., a_p$ are the coefficients associated and the highest power of `x` is `p`.