.. PyTorch Introduction documentation master file, created by
   sphinx-quickstart on Sun Sep 20 02:13:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyTorch Introduction's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Chapter1.rst
   Chapter2.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
